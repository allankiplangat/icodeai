# IcodeAI Landing Page

On Demand DataScience, Machine Learning and Deep Learning Learning MVP (Landing Page)

## Getting Started

Install node and npm on your machine

### clone the repository

clone the repository from the link below

```
git clone https://allankiplangat@bitbucket.org/allankiplangat/icodeai.git
```

### Installing dependancies and running the server

Change directory to the cloned repository using your terminal

```
npm install
```

After all the depandancies have been installed run the command below 


```
npm run serve
```

The build by running the command 


```
npm run build
```

## Confirm the build process has worked by running node server

```
node server
```

## Open Browser

```
localhost:3003
```
